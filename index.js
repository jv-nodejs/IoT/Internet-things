'use strict'

const setupDatabase = require('.lib/db')
const setupAgentModel = require('./models/agent')
const setupMetricModel = require('./models/metric')

module.exports = async (config) => {
  const sequelize = setupDatabase(config)
  const AgentModel = setupAgentModel(config)
  const MetricModel = setupMetricModel(config)
  AgentModel.hasMany(MetricModel)
  MetricModel.belongsTo(AgentModel)
  await sequelize.authunticate()
  // sequelize.sync()

  const Agent = {}
  const Metric = {}

  return {
    Agent,
    Metric
  }
}
